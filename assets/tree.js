(function($, undefined) {

	var colors = ['silver', 'purple'];

	// Tree object definition
	var Tree = {
		body: '',
		$bodyHtml: '',
		buildFromString: function(str) {
			this.body = str;
			this.$bodyHtml = this.convertToHtml(str);
			return this;
		},
		convertToHtml: function(str) {

			var lines = str.split('\n'),
				// Keep tree rows (lines) in list
				$ul = $('<ul></ul>', {
					'id': 'tree',
				}),
				// Create span wrapper with star to replace raw text '*'
				$star = $('<span></span>', {
					text: '*',
				});

			$star.toHtml = function(color) {
				var $self = this;
				// Set random color
				if ( color ) 
					$self.attr('class', color);
				else
					$self.attr('class', colors[Math.floor(Math.random()*colors.length)]);
				return $self[0].outerHTML;
			} 

			// Build html list of tree rows
			$.each(lines, function(i, line) {

				$li = $('<li></li>').text(line); 

				if ( !i ) {
					$li.addClass('top');
					// Append colorful star
					$li.html(function(i, html) {
						return $star.toHtml('gold');
					});
				}
				else if ( i+(lines.length/5)  > lines.length ) {
					$li.addClass('trunk');
				}
				else {
					$li.addClass('body');

					// Wrapp stars with <span> tag
					$li.html(function(i, html) {
						var star_match = html.match(/\*/g);
						var star_count = ( star_match ) ? star_match.length : 0;
						for ( j=0; j < star_count; j++) {
							var chars = $.map(html.split(""), function(char, i) {
								return ( char == '*' ) ? $star.toHtml() : char;  
							});
							html = chars.join("");
						}  
						return html;
					});
				}


				$ul.append($li);
			});


			return $ul;
		},
		toString: function() {
			return this.body;	
		},
		toHtml: function() {
			return this.$bodyHtml;	
		}  
	};

	// Main method
	$(function() {
		// Get tree wrapper
		var $treeWrapper = $('#tree-wrapper');

		// Get and draw christmas tree
		function displayTree() {	
			$.getJSON('./get_json_tree.php', function(data) {
					var tree = Tree.buildFromString(data.tree);
					// Apend tree to document
					$treeWrapper.html(tree.toHtml());
			});
		} 

		// The Neverending story
		displayTree();
		setInterval(displayTree, 1000);

	});
})(jQuery);