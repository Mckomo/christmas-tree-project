#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>

class ChristmasTree {
private:
    enum RowType {
        STAR,
        BODY,
        TRUNK
    };
    
    class Row {
    private:
        std::string characters;
        RowType type;
        int center;
        
        void generateStars() {
            for(int i = 0; i < characters.length()/4; i++) {
                int random = std::rand() % characters.length();
                characters[random] ='*';
            }
            
        }
    public:
        Row(RowType t, int width = 1) {
            type = t;
            
            switch (type) {
                case STAR:
                    characters = "*";
                    center = 0;
                    break;
                    
                case BODY:
                    characters.insert(characters.end(), width, '/');
                    characters.insert(characters.end(), '|');
                    characters.insert(characters.end(), width, '\\');
                    
                    generateStars();
                    center = width;
                    break;
                    
                case TRUNK:
                    characters.insert(characters.begin(), 2*width+1, '|');
                    center = width;
                    break;
            }
            
        }
        friend std::ostream& operator<<(std::ostream&, const Row&);
        
        bool operator<(const Row& right) const {
            return (characters.length() < right.characters.length());
        }
    public:
        int getCenter() const {
            return center;
        }
    };
    
    friend std::ostream& operator<<(std::ostream&, const Row&);
    std::vector<Row> rows;
    
public:
    ChristmasTree(int height) {
        rows.push_back(Row(STAR));
        
        for (int i = 0; i <= height; i++) {
            rows.push_back(Row(BODY, i));
        }
        for (int i = 0; i <= height/5; i++) {
            rows.push_back(Row(TRUNK, height/8));
        }
        
    }
    friend std::ostream& operator<<(std::ostream&, const ChristmasTree&);
private:
    
    void output(std::ostream& o) const {
        
        int maxCenter = std::max_element(rows.begin(),rows.end())->getCenter();
        for (std::vector<Row>::const_iterator it = rows.begin(); it != rows.end(); it++) {
            int currentCenter = it->getCenter();
            if(currentCenter < maxCenter) {
                o << std::setw(maxCenter - currentCenter) << " ";
                
            }
            o << *it << std::endl;
        }
    }
};

std::ostream& operator<<(std::ostream& o, const ChristmasTree::Row& row) {
    return o << row.characters;
}

std::ostream& operator<<(std::ostream& o, const ChristmasTree& tree) {
    tree.output(o);
    return o;
}

int main(int argc, char **argv) {
    
    std::srand ( time(NULL) );
    
    ChristmasTree t(19);
    std::cout<<t;
    return 0;
}