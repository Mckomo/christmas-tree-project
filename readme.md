Website of christmas tree animation. Used: cpp + php + js. 

# Instructions

1. Compile tree generator. Ex. "g++ ./assets/tree_generator.cpp -o ./exec/tree_generator
2. You ready to go

# Changelog:

0.1 Version alpha - 20-12-2012
* Achieved basic functionality 

0.1 Version beta - 21-12-2012
* Added colorful stars