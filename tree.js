(function($, undefined) {

	// Tree object definition
	var Tree = {
		body: '',
		bodyHtml: '',
		buildFromString: function(str) {
			this.body = str;
			this.bodyHtml = str.replace( /\n/g, '<br />\n' );
			return this;
		},
		convertToHtml: function() {
			
		},
		toString: function() {
			return this.body;	
		},
		toHtml: function() {
			return this.bodyHtml;	
		}  
	}; 

	// Main method
	$(function() {
		// Get tree wrapper
		var $treeWrapper = $('#tree-wrapper');

		// The Neverending story
		setInterval(function() {
			$.getJSON('./get_json_tree.php', function(data) {
				var tree = Tree.buildFromString(data.tree);

				// Apend tree to document
				$treeWrapper.html(tree.toHtml());
			});
		}, 1000);

	});
})(jQuery);